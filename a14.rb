class Question
	attr_reader :question
	attr_reader :answer
	def initialize(question,answer)
		@question = question
		@answer = answer
	end
end

def ask(a)
	puts a.question
	if gets.to_i == a.answer
		puts "Correct"
		return 1
	else
		puts "Wrong"
		return 0
	end
end
a = Question.new("What is 2 + 2?", 4)
b = Question.new("What is 2 + 3?", 5)
c = Question.new("What is 2 + 4?", 6)
d = Question.new("What is 2 + 5?", 7)
e = Question.new("What is 2 + 6?", 8)
score = 0
x = [a,b,c,d,e]
for i in 0..4
	score += ask(x[i])
end

z = score*100/5
puts "Your score is #{z}%"