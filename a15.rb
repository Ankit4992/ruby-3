class Question
	attr_reader :question
	attr_reader :answer
	def initialize(question,answer)
		@question = question
		@answer = answer
	end

	def ask
		puts @question
		if gets.to_i == @answer
			puts "Correct"
		else
			puts "Wrong"
		end
	end
end


a = Question.new("What is 2 + 2?", 4)
a.ask