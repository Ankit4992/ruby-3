def add(number_of_questions)
	puts "Ask #{number_of_questions} addition questions"
	number_of_questions.times do|i|
		a = rand(10)
		b = rand(10)
		puts "Q#{i+1}: What is #{a} + #{b} ?"
	end
end
def substract(number_of_questions)
	puts "Ask #{number_of_questions} addition questions"
	number_of_questions.times do|i|
		a = rand(10)
		b = rand(10)
		puts "Q#{i+1}: What is #{a} - #{b} ?"
	end
end
operation_to_perform = ARGV[0]
number_of_questions = ARGV[1].to_i

if operation_to_perform == "add"
	add(number_of_questions)
else
	substract(number_of_questions)
end