class Question
	attr_reader :question
	attr_reader :answer
	def initialize(question,answer)
		@question = question
		@answer = answer
	end
end

def ask(a)
	puts a.question
	if gets.to_i == a.answer
		puts "Correct"
	else
		puts "Wrong"
	end
end
a = Question.new("What is 2 + 2?", 4)
ask(a)